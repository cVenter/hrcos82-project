[QueryGroup="Customers"] @collection [[
[QueryItem="Get All Customers"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?email ?name ?address ?customer WHERE {?customer a :Customer; :customerName ?name ; :customerAddress ?address ; :customerEmail ?email}

[QueryItem="Get hosting customers"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customer ?name ?address ?email WHERE {?customer a :HostingServiceCustomer ; :hostingServiceCustomerName ?name ; :hostingServiceCustomerAddress ?address ; :hostingServiceCustomerEmail ?email. ?customer :subscribesToHostingService ?service. }

[QueryItem="Access customers"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customer ?name ?address ?email WHERE {?customer a :AccessServiceCustomer. ?customer :accessServiceCustomerName ?name. ?customer :accessServiceCustomerAddress ?address. ?customer :accessServiceCustomerEmail ?email. ?customer :subscribesToAccessService ?service }

[QueryItem="Data customers"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customer ?name ?address ?email WHERE {?customer a :DataServiceCustomer ; :dataServiceCustomerName ?name ; :dataServiceCustomerAddress ?address ; :dataServiceCustomerEmail ?email. ?customer :subscribesToDataService ?service }

[QueryItem="Hosting and Data Customers"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customer ?name ?address ?email WHERE {?customer a :Customer ; :customerName ?name ; :customerAddress ?address ; :customerEmail ?email. ?customer :subscribesToDataService ?dataService. ?customer :subscribesToHostingService ?hostingService. }

[QueryItem="Data and Access Customers"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customer ?name ?address ?email WHERE {?customer a :Customer ; :customerName ?name ; :customerAddress ?address ; :customerEmail ?email. ?customer :subscribesToDataService ?dataService. ?customer :subscribesToAccessService ?accessService. }

[QueryItem="Customers who subscribe to all services"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customer ?name ?address ?email WHERE {?customer a :Customer ; :customerName ?name ; :customerAddress ?address ; :customerEmail ?email. ?customer :subscribesToDataService ?dataService. ?customer :subscribesToAccessService ?accessService. 
?customer :subscribesToHostingService ?hostingService. }

[QueryItem="Hosting only customers"]
SELECT  view1.customer, FROM etable (
   PREFIX : <http://www.44618042.org/ispOntology#>
   SELECT ?customer ?name ?address ?email ?accessService ?hostingService WHERE {
     ?customer a :Customer ; :customerName ?name ; :customerAddress 
        ?address ; :customerEmail    ?email. 
     ?customer :subscribesToAccessService ?accessService. 
     ?customer :subscribesToAccessService ?accessService. 
     ?customer :subscribesToHostingService ?hostingService.  
   }
) view1 WHERE view1.hostingService is null and view1.accessService is null
]]

[QueryGroup="Services"] @collection [[
[QueryItem="All service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?service a :Service; :serviceType ?type. }

[QueryItem="Hosting services"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?service a :HostingService; :hostingServiceType ?type. }

[QueryItem="Data services"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?service a :DataService; :dataServiceType ?type. }

[QueryItem="Access services"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?service a :AccessService; :accessServiceType ?type. }
]]

[QueryGroup="Servers"] @collection [[
[QueryItem="All servers"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?server a :Server; :serverName ?name; :serverType ?type. }
]]

[QueryGroup="Domains"] @collection [[
[QueryItem="All domains"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?domain a :WebDomain; :domainName ?name. }

[QueryItem="Registered domains"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?domain a :RegisteredDomain; :registeredDomainName ?name. }

[QueryItem="Hosted domains"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?domain a :HostedDomain; :hostedDomainName ?name. }

[QueryItem="Hosted and registered domains"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?domain a :RegisteredDomain. ?domain a :HostedDomain.}
]]

[QueryGroup="Circuits"] @collection [[
[QueryItem="All Circuits"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?circuit WHERE {?circuit a :Circuit. ?circuit :circuitNum ?circuit_num }

[QueryItem="Access circuits"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?circuit WHERE {?circuit :accessCircuitId ?circuitId.}

[QueryItem="Connected circuits"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?circuit WHERE {?circuit :connectedCircuitId ?circuitId.}

[QueryItem="Access circuits connected to data service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?circuit WHERE {?circuit :connectedCircuitNum ?circuitNum. ?circuit :accessCircuitNum ?circuitNum. }
]]

[QueryGroup="Data Packages"] @collection [[
[QueryItem="All data packages"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT * WHERE {?package a :DataPackage ; :dataAmountKB ?dataAmount ; :capped ?capped ; :recurringPeriod ?recurringPeriod. }
]]

[QueryGroup="Service Domain"] @collection [[
[QueryItem="Service provides domain"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?service ?domain WHERE {?service :hostingServiceType ?serviceType. ?domain a :HostedDomain. ?service :provides ?domain. }

[QueryItem="Domain provided by service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?domain ?service WHERE {?service :hostingServiceType ?serviceType. ?domain a :HostedDomain. ?domain :providedBy ?service. }
]]

[QueryGroup="Customer-Service"] @collection [[
[QueryItem="Customer  subscribes to hosting service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customerEmail ?hostingService WHERE {?customer :subscribesToHostingService ?hostingService. ?customer :hostingServiceCustomerEmail ?customerEmail }

[QueryItem="Hosting service subscribed by customer"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?hostingService ?customerEmail WHERE {?hostingService :hostingServiceSubscribedBy ?customer. ?customer :hostingServiceCustomerEmail ?customerEmail }

[QueryItem="Customer subscribes to data service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customerEmail ?dataService WHERE {?customer :subscribesToDataService ?dataService. ?customer :hostingServiceCustomerEmail ?customerEmail. }

[QueryItem="Data service subscribed by customer"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?dataService ?customerEmail WHERE {?dataService :dataServiceSubscribedBy ?customer. ?customer :dataServiceCustomerEmail ?customerEmail }

[QueryItem="Customer subscribes to access service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?customerEmail ?accessService WHERE {?customer :subscribesToAccessService ?accessService. ?customer :accessServiceCustomerEmail ?customerEmail. }

[QueryItem="Access service subscribed by customer"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?accessService ?customerEmail WHERE {?accessService :accessServiceSubscribedBy ?customer. ?customer :accessServiceCustomerEmail ?customerEmail }
]]

[QueryGroup="Data Service-Data Package"] @collection [[
[QueryItem="Data service provides package"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?service ?package WHERE {?service :providesData ?package. }

[QueryItem="Data package provided by service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?package ?service WHERE {?package :providedByData ?service. }
]]

[QueryGroup="Service-Circuit"] @collection [[
[QueryItem="Access service provides circuit"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?service ?package WHERE {?service :providesCircuit ?package. }

[QueryItem="Circuit provided by access service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?circuit ?service WHERE {?circuit :providedByAccess ?service. }

[QueryItem="Data service connects to circuit"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?service ?circuit WHERE {?service :connectedTo ?circuit. }

[QueryItem="Circuit connected by data service"]
PREFIX : <http://www.44618042.org/ispOntology#>
SELECT ?circuit ?service WHERE {?circuit :connectedBy ?service. }
]]
